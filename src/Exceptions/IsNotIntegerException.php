<?php

namespace Exceptions;

/**
 * Class IsNotIntegerException
 * 
 * The exception class to throw when some param is not a number
 * 
 * 
 * @package Exceptions
 * 
 * @version 1.0
 * 
 */
class IsNotIntegerException extends \Exception
{
}
